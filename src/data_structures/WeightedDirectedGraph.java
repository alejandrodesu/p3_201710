package data_structures;

import java.util.*;


import data_structures.NodeVertice;
public class WeightedDirectedGraph <K extends Comparable<K> , V> {

	private int E;                 
	private EncadenamientoSeparadoTH <K , NodeVertice <K, V>> vertices;    

	public WeightedDirectedGraph(int vertices){
		this.E = 0;
		this.vertices = new EncadenamientoSeparadoTH <K, NodeVertice <K, V>> (vertices);
	}

	public int numVertices(){
		return vertices.darTamanio();
	}

	public Iterable <DirectedEdge<K>> adj(K  id)throws NoSuchElementException{
		NodeVertice <K, V> vertice = vertices.darValor(id);
		if (vertice == null)
			throw new NoSuchElementException();
		return vertice.adj();
	}

	public V darVertice(K id) throws NoSuchElementException{
		NodeVertice <K, V> vertice = vertices.darValor(id);
		if (vertice == null)
			throw new NoSuchElementException();
		return vertice.item;
	}

	public void agregarVertice(K id, V
			infoVer){
		NodeVertice <K, V> node = new NodeVertice<K, V>(id, infoVer); 
		vertices.insertar(id, node);
	}

	public int numArcos(){
		return E;
	}

	public double darPesoArco(K idOrigen, K idDestino) throws NoSuchElementException {
		NodeVertice <K, V> node = vertices.darValor(idOrigen);
		if (node != null)
			for (DirectedEdge<K> edge: node.adj()){
				if (edge.to().equals(idDestino))
					return edge.weight();
			}
		throw new NoSuchElementException();
	}

	public void agregarArco(K idOrigen, K
			idDestino, double peso){
		agregarArco(idOrigen, idDestino, peso, Character.MAX_VALUE);
	}

	public void agregarArco (K idOrigen, K
			idDestino, double peso, char
			ordenLexicografico)  throws NoSuchElementException{
		NodeVertice <K, V> node = vertices.darValor(idOrigen);
		if (node != null)
			E += node.agregaEdge(idOrigen, idDestino, peso, ordenLexicografico);
		else
			throw new NoSuchElementException();
	}

	public Iterator<K> darVertices(){
		return vertices.keys();
	}

	public int darGrado(K id) throws NoSuchElementException{
		NodeVertice <K, V> node = vertices.darValor(id);
		if (node != null)
			return node.grado;
		throw new NoSuchElementException();
	}

	public Iterator<K> darVerticesAdyacentes(K id) throws NoSuchElementException{
		NodeVertice <K, V> node = vertices.darValor(id);
		if (node != null)
			return node.adj().iteratorKeys();
		throw new NoSuchElementException();
	}

	public void desmarcar(){
		for (NodeVertice <K, V> nodo : vertices)
			nodo.marcado = false;
	}
	public boolean contiene (K id){
		return vertices.darValor(id) != null;
	}

	public EncadenamientoSeparadoTH <K,NodoCamino<K>> DFS(K idOrigen) throws NoSuchElementException{
		NodeVertice <K, V> node = vertices.darValor(idOrigen);
		if (node != null){
			node.marcado = true;
			EncadenamientoSeparadoTH <K,NodoCamino<K>> tabla = new EncadenamientoSeparadoTH <K,NodoCamino<K>>(numVertices());
			tabla.insertar(idOrigen, new NodoCamino<K>(idOrigen, idOrigen, 0, 0));
			for (DirectedEdge <K> edge : node.adj()){
				int longitud = 0;
				double peso = 0.0;
				NodeVertice <K, V> nodeInterno = vertices.darValor(edge.to());
				if (!nodeInterno.marcado)
					dfs (nodeInterno, edge, tabla, peso, longitud);
			}
			desmarcar();
			return tabla;
		}
		else
			throw new NoSuchElementException();
	}

	private void dfs (NodeVertice <K, V> node, DirectedEdge <K> edge, EncadenamientoSeparadoTH <K,NodoCamino<K>> tabla, double peso, int longitud){
		NodoCamino<K> nodoCamino = edge.camino(++longitud, peso);
		tabla.insertar(nodoCamino.idFinal, nodoCamino);
		peso = nodoCamino.weight;
		node.marcado = true;

		for (DirectedEdge <K> edgeInterno : node.adj()){
			NodeVertice <K, V> nodeInterno = vertices.darValor(edgeInterno.to());
			if (!nodeInterno.marcado)
				dfs (nodeInterno, edgeInterno, tabla, peso, longitud);
		}
	}

	public EncadenamientoSeparadoTH <K,NodoCamino<K>> BFS(K idOrigen){
		NodeVertice <K, V> node = vertices.darValor(idOrigen);
		if (node != null){
			EncadenamientoSeparadoTH <K,NodoCamino<K>> tabla = new EncadenamientoSeparadoTH <K,NodoCamino<K>>(numVertices());
			bfs (node, tabla);
			desmarcar();
			return tabla;
		}
		else
			throw new NoSuchElementException();
	}

	private void bfs (NodeVertice <K, V> node,  EncadenamientoSeparadoTH <K,NodoCamino<K>> tabla){
		QueueLLaveValor <Double, NodeVertice <K, V>> cola2 = new QueueLLaveValor <Double, NodeVertice <K, V>>();
		tabla.insertar(node.key, new NodoCamino<K>(node.key, node.key, 0.0, 0));
		cola2.enqueue(0.0, node);
		node.marcado = true;
		int longitud = 1;

		while (!cola2.isEmpty()){
			int tamanioCola = cola2.size();
			for (int i = 0; i < tamanioCola; i++){
				double peso = cola2.verKey();
				NodeVertice <K, V> nodoInterno = cola2.dequeue();

				for (DirectedEdge <K> edgeInterno : nodoInterno.adj()){
					NodeVertice <K, V> nodoInterno2 = vertices.darValor(edgeInterno.to());
					if (!nodoInterno2.marcado){
						NodoCamino<K> nodoCamino = edgeInterno.camino(longitud, peso);
						tabla.insertar(nodoInterno2.key, nodoCamino);
						nodoInterno2.marcado = true;
						cola2.enqueue(nodoCamino.weight, nodoInterno2);
					}
				}
			}
			longitud++;
		}
	}

	public Iterable <NodoCamino<K>> darCaminoDFS(K
			idOrigen, K idDestino){
		return darCamino(idOrigen, idDestino, DFS(idOrigen));
	}

	public Iterable <NodoCamino<K>> darCaminoBFS (K
			idOrigen, K idDestino){
		return darCamino(idOrigen, idDestino, BFS(idOrigen));
	}

	public Iterable <NodoCamino<K>> darCaminoDijkstra (K
			idOrigen, K idDestino){
		return darCamino(idOrigen, idDestino, dijkstra(idOrigen));
	}

	private Iterable <NodoCamino<K>> darCamino(K
			idOrigen, K idDestino, EncadenamientoSeparadoTH <K,NodoCamino<K>> camino){
		Stack <NodoCamino<K>> stack = new Stack<>();
		if (camino.tieneLlave(idDestino)){
			while (idDestino != idOrigen){
				NodoCamino<K> nodoCamino = camino.darValor(idDestino);
				stack.push(nodoCamino);
				idDestino = nodoCamino.idAdy;
			}
		}else
			return null;
		return stack;
	}

	public EncadenamientoSeparadoTH<K , NodoCamino<K>> dijkstra (K idOrigen){
		EncadenamientoSeparadoTH<K , NodoCamino<K>> tabla = new EncadenamientoSeparadoTH<> (numVertices());
		IndexMinHeap <K, NodoCamino<K>> pq = new IndexMinHeap <>(numVertices());

		pq.agregar(idOrigen, new NodoCamino<K>(idOrigen, idOrigen, 0, 0), true);
		while (!pq.esVacia()){
			K v = pq.minKeyVer();
			NodoCamino<K> nodoCamino = pq.min();
			vertices.darValor(v).marcado = true;
			tabla.insertar(v, nodoCamino);
			for (DirectedEdge<K> edge : adj(v)){
				K w = edge.to();
				if (!vertices.darValor(w).marcado){
					NodoCamino<K> nodoCaminoInterno = edge.camino(nodoCamino.lenght+1, nodoCamino.weight);
					pq.agregar(w, nodoCaminoInterno, true);
				}
			}
		}
		desmarcar();
		return tabla;
	}
}
